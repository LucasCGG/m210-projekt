USE quizme_db;

DROP TABLE IF EXISTS language;

CREATE TABLE language (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    flag VARCHAR(50)
);

INSERT INTO language (name, flag) VALUES ('Englisch', 'GB'), 
('Deutsch', 'DE'), 
('Franzoesisch', 'FR'), 
('Spanisch', 'ES'), 
('Italienisch', 'IT');
